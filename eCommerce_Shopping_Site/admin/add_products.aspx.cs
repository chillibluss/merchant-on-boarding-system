﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace eCommerce_Shopping_Site.admin
{
    public partial class add_products : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\eCommerce_Shopping_Site\eCommerce_Shopping_Site\App_Data\eCommerce_Admin_Module.mdf;Integrated Security=True");
        string a, b;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"] == null)
            {
                Response.Redirect("admin_Login.aspx");
            }

            if (IsPostBack) return;
            dd.Items.Clear();

            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from productcategory";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                dd.Items.Add(dr["product_category"].ToString());
            }
            con.Close();
        }

        protected void b1_Click(object sender, EventArgs e)
        {
            int rows;
            Random rnd = new Random();
            int a = rnd.Next(1, 1000);
            f1.SaveAs(Request.PhysicalApplicationPath + "./images/" + a + f1.FileName.ToString());
            b = "images/" + a + f1.FileName.ToString(); 
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into Items values('"+t1.Text+"','"+t2.Text+"',"+t3.Text+","+t4.Text+",'"+b.ToString()+ "','" + dd.SelectedItem.ToString() + "')";
            rows = cmd.ExecuteNonQuery();
            if(rows>0)
            Response.Write("<script>alert('Item added successfully');</script>");
            else
                Response.Write("<script>alert('Try Again!');</script>");

        }
    }
}