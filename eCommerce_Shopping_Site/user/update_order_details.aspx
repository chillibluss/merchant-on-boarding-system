﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/user.Master" AutoEventWireup="true" CodeBehind="update_order_details.aspx.cs" Inherits="eCommerce_Shopping_Site.user.update_order_details" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" runat="server">
    <h1>ORDER CONFIRMATION</h1><br />
    <asp:DataList ID="d1" runat ="server">
            <HeaderTemplate>
                <table border ="1">
                    <tr style="background-color:silver; color:white; font-weight:bold">
                        <td>Product Image</td>
                        <td>Product Name</td>
                        <td>Product Description</td>
                        <td>Product Price</td>
                        <td>Product Quantity</td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <img src ="../<%#Eval("product_images")%>" height="100" width="100" />
                    </td>
                    <td>
                        <%#Eval("product_name")%>
                    </td>
                    <td>
                        <%#Eval("product_desc")%>
                    </td>
                    <td>
                        <%#Eval("product_price")%>
                    </td>
                    <td>
                        <%#Eval("product_qty")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:DataList>
    <table>
        <tr>
            <td>
                First Name :
            </td>
            <td>
                <asp:TextBox ID="t1" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Last Name :
            </td>
            <td>
                <asp:TextBox ID="t2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Shipping Address :
            </td>
            <td>
                <asp:TextBox ID="t3" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                City :
            </td>
            <td>
                <asp:TextBox ID="t4" runat="server" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                State :
            </td>
            <td>
                <asp:TextBox ID="t5" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>
                Mobile :
            </td>
            <td>
                <asp:TextBox ID="t6" runat="server"></asp:TextBox>
            </td>
        </tr>
            <tr>
            <td colspan ="2" align="center">
                <asp:Label ID="l1" runat="server"></asp:Label> <br />
            </td>
        </tr>
        <tr>
            <td colspan ="2" align="center">
                <asp:Button ID="b1" runat="server" Text ="Checkout" OnClick="b1_Click"/>
            </td>
        </tr>
    </table>
</asp:Content>
