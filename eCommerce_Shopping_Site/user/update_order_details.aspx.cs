﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace eCommerce_Shopping_Site.user
{
    public partial class update_order_details : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\eCommerce_Shopping_Site\eCommerce_Shopping_Site\App_Data\eCommerce_Admin_Module.mdf;Integrated Security=True");
        string s;
        string t;
        string[] a = new string[6];
        int total = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["user"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (IsPostBack)
            { return; }
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7] { new DataColumn("product_name"), new DataColumn("product_desc"),
                                                   new DataColumn("product_price"), new DataColumn("product_qty"),
                                                   new DataColumn("product_images"),new DataColumn("id"),new DataColumn("product_id") });
            if (Request.Cookies["aa"] != null)
            {
                s = Convert.ToString(Request.Cookies["aa"].Value);

                string[] strArr = s.Split('|');

                for (int i = 0; i < strArr.Length; i++)
                {
                    t = Convert.ToString(strArr[i].ToString());
                    string[] strArr1 = t.Split(',');
                    for (int j = 0; j < strArr1.Length; j++)
                    {
                        a[j] = strArr1[j].ToString();
                    }
                    dt.Rows.Add(a[0].ToString(), a[1].ToString(), a[2].ToString(), a[3].ToString(),
                        a[4].ToString(),i.ToString(),a[5].ToString());
                    total = total + (Convert.ToInt32(a[2].ToString()) * Convert.ToInt32(a[3].ToString()));
                }

            }
            d1.DataSource = dt;
            d1.DataBind();
            l1.Text = "Total : " + total.ToString() + "INR";

            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from registration where email='" + Session["user"].ToString() + "'";
            cmd.ExecuteNonQuery();
            DataTable dt1 = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt1);
            foreach (DataRow dr in dt1.Rows)
            {
                t1.Text = dr["firstname"].ToString();
                t2.Text = dr["lastname"].ToString();
                t3.Text = dr["address"].ToString();
                t4.Text = dr["city"].ToString();
                t5.Text = dr["state"].ToString();
                t6.Text = dr["mobile"].ToString();
            }
            con.Close();
          
        }

        protected void b1_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update registration set firstname='" +t1.Text+ "',lastname='"+ t2.Text + "',address='"+ t3.Text + "',city='"+ t4.Text + "',state='"+ t5.Text + "',mobile='"+ t6.Text + "' where email='"+Session["user"].ToString()+"'";
            cmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("payment_gateway.aspx");
        }
    }
}