﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="add_products.aspx.cs" Inherits="eCommerce_Shopping_Site.admin.add_products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="c1" runat="server">
    
    <h3> Add Product </h3>

    <table>
        <tr>
            <td>Product name</td>
            <td><asp:TextBox ID ="t1" runat="server">   </asp:TextBox></td>
        </tr>
        <tr>
            <td>Product Description</td>
            <td><asp:TextBox ID ="t2" runat="server">   </asp:TextBox></td>
        </tr>
        <tr>
            <td>Product price</td>
            <td><asp:TextBox ID ="t3" runat="server">   </asp:TextBox></td>
        </tr>
        <tr>
            <td>Product Quantity</td>
            <td><asp:TextBox ID ="t4" runat="server">   </asp:TextBox></td>
        </tr>
        <tr>
            <td>Product Image</td>
            <td><asp:FileUpload ID="f1" runat="server"/></td>
        </tr>
        <tr>
            <td>Product Quantity</td>
            <td><asp:DropDownList ID ="dd" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="2" align ="center">
                <asp:Button ID="b1" runat="server" Text="Upload" OnClick="b1_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
