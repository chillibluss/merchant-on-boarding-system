﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eCommerce_Shopping_Site.admin
{
    public partial class admin_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void b1_Click(object sender, EventArgs e)
        {
            if (t1.Text == "admin" && t2.Text == "admin")
            {
                Session["admin"] = t1.Text;
                Response.Redirect("add_products.aspx");
            }
            else
            {
                label1.Text = "You have entered invalid credentials";
            }
        }
    }
}