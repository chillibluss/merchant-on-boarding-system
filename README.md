The first page to run the project will be - display_item.aspx (for user side) , add_products.aspx (for admin side)

Admin can add and delete products to the website.He can also view all the orders.
admin credentials - username - admin   password -  admin  

User have functionalities like:
.Home 
    -Show list of items configured for the Shopping site.			
.View Item
    -View details of the selected item -
    -Ability to add to Shopping cart.	
.Cart 
    -Show list of items selected by user
    -Option to increase/decrease quantity
    -Remove the item from cart
.Checkout 
    -Show list of items selected by user
    -Option to access Cart
    -Option to provide Billing/Shipping details
    -Order Confirmation Screen
    -Ability to navigate to the home page from the current screen.
    
user credentials(to see existing orders) :
username      password
a@a             a
user@user      user

    Yet to implement payment gateway for the user.
