﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace eCommerce_Shopping_Site.user
{
    public partial class display_item : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\eCommerce_Shopping_Site\eCommerce_Shopping_Site\App_Data\eCommerce_Admin_Module.mdf;Integrated Security=True");

        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;

            if (Request.QueryString["category"] == null)
            {
                cmd.CommandText = "select * from Items";
            }
            else
            {
                cmd.CommandText = "select * from Items where product_category='"+ Request.QueryString["category"].ToString()+"'";
            }
            
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            d1.DataSource = dt;
            d1.DataBind();
            con.Close();
            

        }
    }
}